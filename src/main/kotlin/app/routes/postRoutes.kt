package app.routes

import app.dao.PostDAO
import app.middlewares.loggerMiddleware
import app.validators.PostCreatePayload
import app.validators.PostCreateValidator
import app.validators.PostUpdatePayload
import app.validators.PostUpdateValidator
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Application.postRoutes() {
    routing {
        val postDAO = PostDAO()

        get("/posts") {
            call.respond(mapOf("posts" to postDAO.all()))
        }

        get("/posts/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            call.respond(mapOf("post" to postDAO.show(id)))
        }

        post("/posts") {
            val validatedPost = PostCreateValidator.validate(call.receive<PostCreatePayload>())
            val hardcodedUserId = 1 // get the id of the logged in user
            val newPost = postDAO.create(hardcodedUserId, validatedPost.title, validatedPost.body)
            call.respond(mapOf("post" to newPost))
        }

        put("/posts/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val validatedPost = PostUpdateValidator.validate(call.receive<PostUpdatePayload>())
            val updatedPost = postDAO.update(id, validatedPost.title, validatedPost.body)
            call.respond(mapOf("post" to updatedPost))
        }

        delete("/posts/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val deletedPost = postDAO.delete(id)
            call.respond(mapOf("post" to deletedPost))
        }
    }
}