package app.routes

import app.middlewares.detectUserLocale
import app.middlewares.hasSignedUrlMiddleware
import app.middlewares.loggerMiddleware
import app.plugins.CtxService
import app.plugins.UserService
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject


fun Application.demoRoutes() {
    val ctxService: CtxService by inject()
    val userService: UserService by inject()

    routing {
        get("/") {
            call.respondRedirect("demo")
        }

        get("/demo") {
            loggerMiddleware()
            call.respond(mapOf("hello" to "world"))
        }

        get("/demo/text-response") {
            call.respondText("This is a text response from demoRoutes")
        }

        get("/demo/signed-url") {
            // you must add the url param ?signature=1234 for this route to respond successfully
            hasSignedUrlMiddleware(context as ApplicationCall)
            call.respondText("This is a text response from /demo/signed-url")
        }

        get("/demo/translation") {
            detectUserLocale(context as ApplicationCall)
            call.respondText("This is a text response from demoRoutes")
        }

        get("/demo/koin/ctx") {
            call.respond(mapOf("context" to ctxService.getCtx()))
        }

        get("/demo/koin/me") {
            call.respond(mapOf("me" to userService.me()))
        }
    }
}