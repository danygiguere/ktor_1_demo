package app.routes

import app.dao.UserDAO
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Application.userRoutes() {
    routing {
        val userDAO = UserDAO()

        get("/users") {
            call.respond(mapOf("users" to userDAO.all()))
        }

        get("/users/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            call.respond(mapOf("user" to userDAO.show(id)))
        }
    }
}