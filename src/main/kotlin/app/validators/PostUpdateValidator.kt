package app.validators

import app.plugins.UnprocessableEntityException
import kotlinx.serialization.Serializable

@Serializable
data class PostUpdatePayload(
    val title: String,
    val body: String
)

object PostUpdateValidator {
    fun validate(payload: PostUpdatePayload): PostUpdatePayload {
        val titleArray = mutableListOf<String>()
        val bodyArray = mutableListOf<String>()
        if (payload.title.length < 20) {
            titleArray.add("The title must contain at least 20 characters")
        }
        if (payload.title.length > 100) {
            titleArray.add("The title must contain no more than 100 characters")
        }
        if (payload.body.length < 20) {
            bodyArray.add("The body must contain at least 20 characters")
        }
        if (titleArray.size > 0) {
            throw UnprocessableEntityException(
                mapOf(
                    "title" to titleArray,
                    "body" to bodyArray
                )
            )
        }
        return payload
    }
}