package app.validators

import app.plugins.UnprocessableEntityException
import kotlinx.serialization.Serializable

@Serializable
data class PostCreatePayload(
    val title: String,
    val body: String
)

object PostCreateValidator {
    fun validate(payload: PostCreatePayload): PostCreatePayload {
        val titleArray = mutableListOf<String>()
        val bodyArray = mutableListOf<String>()
        if (payload.title.length < 10) {
            titleArray.add("The title must contain at least 10 characters")
        }
        if (payload.title.length < 10) {
            titleArray.add("Another failed validation reason for this field")
        }
        if (payload.body.length < 10) {
            bodyArray.add("The body must contain at least 10 characters")
        }
        if (payload.body.length < 10) {
            bodyArray.add("Another failed validation reason for this field")
        }
        if (titleArray.size > 0) {
             throw UnprocessableEntityException(
                  mapOf(
                      "title" to titleArray,
                      "body" to bodyArray
                  )
             )
        }
        return payload
    }
}