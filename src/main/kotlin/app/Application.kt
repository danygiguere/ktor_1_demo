package app

import app.config.Database
import app.plugins.*
import io.ktor.application.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module(testing: Boolean = false) {
    configureExceptions()
    configureKoin()
    Database.init()
    configureSerialization()
    configureRouting()
    runSeeders()
}



