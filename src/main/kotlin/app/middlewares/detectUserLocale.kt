package app.middlewares

import io.ktor.application.*
import io.ktor.request.*
import io.ktor.routing.*

fun Application.detectUserLocale(context: ApplicationCall) {
    routing {
        val acceptLanguage = context.request.acceptLanguage()
        if(acceptLanguage == "fr") {
            log.info("french")
        } else {
            log.info("english")
        }

    }
}