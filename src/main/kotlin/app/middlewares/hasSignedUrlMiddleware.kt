package app.middlewares

import app.plugins.AuthorizationException
import io.ktor.application.*
import io.ktor.routing.*

fun Application.hasSignedUrlMiddleware(context: ApplicationCall) {

    routing {
        val signature = context.request.queryParameters["signature"]
        log.info(signature)
        if(signature != "1234") {
            throw AuthorizationException("Sorry you are not authorized")
        }
    }
}