package app.middlewares

import io.ktor.application.*
import io.ktor.routing.*

fun Application.loggerMiddleware() {
    routing {
        log.info("Hello from loggerMiddleware!")
    }
}