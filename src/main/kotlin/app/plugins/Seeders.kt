package app.plugins

import app.seeds.PostSeeder
import app.seeds.UserSeeder
import io.github.cdimascio.dotenv.Dotenv
import io.ktor.application.*

fun Application.runSeeders() {
    val dotenv = Dotenv.load();
    val env = dotenv.get("ENV")
    if (env == "development") {
        UserSeeder().run()
        PostSeeder().run()
    }
}