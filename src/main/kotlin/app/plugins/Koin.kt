package app.plugins

import app.models.User
import io.ktor.application.*
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.KoinApplicationStarted
import org.koin.ktor.ext.KoinApplicationStopPreparing
import org.koin.ktor.ext.KoinApplicationStopped
import org.koin.logger.SLF4JLogger

// Koin: dependency injection for Ktor
fun Application.configureKoin() {
    environment.monitor.subscribe(KoinApplicationStarted) {
        log.info("Koin started.")
    }
    install(Koin) {
        SLF4JLogger()
        modules(userAppModule)
        modules(ctxAppModule)
    }
    environment.monitor.subscribe(KoinApplicationStopPreparing) {
        log.info("Koin stopping...")
    }
    environment.monitor.subscribe(KoinApplicationStopped) {
        log.info("Koin stopped.")
    }
}

interface UserService {
    suspend fun me(): User
}

class UserServiceImpl() : UserService {
    override suspend fun me() = User(
        id = 1,
        username = "johndoe",
        email = "johndoe@test.com",
        password = "secret"
    )
}

val userAppModule = module {
    single<UserService> { UserServiceImpl() }
}


interface CtxService {
    fun getCtx(): ApplicationCall
}

class CtxServiceImpl(val context: ApplicationCall) : CtxService {
    override fun getCtx() = context
}

val ctxAppModule = module {
    single<CtxService> { CtxServiceImpl(get()) }
}