package app.plugins

import app.routes.*
import io.ktor.application.*
import io.ktor.routing.*

fun Application.configureRouting() {

    routing {
        demoRoutes()
        userRoutes()
        postRoutes()
    }
}
