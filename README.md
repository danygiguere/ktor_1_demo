### About
- just a project to learn Ktor 1

### Installation
- do `cp .env.example .env`
- or create a `.env` file and copy the content from `.env.example`. 
- 
- Then add your db credentials to the `.env` file (the `.env` can't be commited because it's in the .gitignore).
- or to use the in-memory db: go to config/Database and follow the commented instructions

- Then, in two different terminals run these commands from the root of this project: 

    - to activate auto reloading:
    `./gradlew -t installDist`

    - to run the app:
    `./gradlew run`

### Info
- to rerun the db migrations, delete the db tables and rerun the app
- to rerun the db seeds, delete the db tables' content and rerun the app
    
### Demo requirements
#### Completed
- create demo, user and post controllers (demoRoutes, userRoutes, postRoutes)
- create Post model
- create User model
- create db migrations
- create db seeds
- create validation for the post model
- add .env for secrets
- create a middleware

#### todo
- create `user has many posts` relationship
- finish implementing Koin
- install i18n
- translate validation
- install JWT
- create demos for joins, db transactions, logs
- figure out a way to remigrate the db and the seeds
- create some tests
- figure how to reset the test db before each test
- figure how to run the tests as transactions, so the data is not commited in the db
- install swagger : https://ktor.io/docs/eap/openapi-swagger.html